#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //ui->setupUi(this);

    QFile file(":qdarkstyle/style.qss");
    if (!file.exists())
    {
        printf("Unable to set stylesheet, file not found\n");
    }
    else
    {
        file.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&file);
        qApp->setStyleSheet(ts.readAll());
    }

    //qDebug() << "test";

    ui->setupUi(this);
    ui->graphicsView->setScene(m_system.getScene());
    settings = m_system.getSettings();
    //planets = sys->getPlanets();

    connect(&timer,SIGNAL(timeout()),this,SLOT(update()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update()
{
    m_system.step();
}

void MainWindow::on_planetAmountSpinBox_valueChanged(int arg1)
{
    settings->amount = static_cast<size_t>(arg1);
}

void MainWindow::on_startButton_clicked()
{
    m_system.createSystem();

    timer.start(16/settings->timescale);
}

void MainWindow::on_pauseButton_clicked()
{
    timer.stop();
}

void MainWindow::on_simulationSpeedSlider_valueChanged(int value)
{
    settings->timescale = static_cast<size_t>(value/100);
    timer.setInterval(16/settings->timescale);
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    settings->seed = static_cast<size_t>(arg1);
}

void MainWindow::on_zoomInButton_clicked()
{
    scaleVal += 1;
    ui->graphicsView->scale(scaleVal, scaleVal);
}

void MainWindow::on_zoomOutButton_clicked()
{
    scaleVal -= 1;
    ui->graphicsView->scale(scaleVal, scaleVal);
}
