#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QTextStream>
#include <QTimer>
#include <QVector2D>
#include <QVector>

#include "system.h"
#include "planet.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_planetAmountSpinBox_valueChanged(int arg1);

    void on_startButton_clicked();

    void on_pauseButton_clicked();

    void update();

    void on_simulationSpeedSlider_valueChanged(int value);

    void on_spinBox_valueChanged(int arg1);

    void on_zoomInButton_clicked();

    void on_zoomOutButton_clicked();

private:
    Ui::MainWindow *ui;

    System m_system;
    SystemSettings* settings;
    //QVector<Planet>* planets;

    QTimer timer;

    double scaleVal = 1;
};

#endif // MAINWINDOW_H
