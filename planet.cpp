#include "planet.h"

Planet::Planet(): mass(0.f) {}

Planet::Planet(float x, float y, float m): mass(m), location(x,y)
{

}

void Planet::interact(Planet &other)
{

    float distance = (other.location - location).length();

    // Used to clamp velocity when planets are too near
    distance = std::max(distance,minDistance);

    // Will calculate a scalar which is used to scale the direction vector (aka. finallocation)
    float force = calculateForce(distance, other.mass);

   other.applyForceFromOtherPlanet(force,location);

    /* Will make a vector which is scaled by the mass of the two planets divided by the distance for a "falloff" */
    velocity+= (other.location - location).normalized() * force * constant;
}

float Planet::calculateForce(float distance, float othermass)
{
    return (mass * othermass) / (distance * distance);
}

void Planet::applyVelocity()
{
    location += velocity;
}

QVector2D Planet::getLocation() const
{
    return location;
}

float Planet::getMass() const
{
    return mass;
}

void Planet::setMass(float value)
{
    mass = value;
}

void Planet::applyForceFromOtherPlanet(float force, QVector2D otherLocation)
{
    velocity+= (otherLocation - location).normalized() * force * constant;
}
