#ifndef PLANET_H
#define PLANET_H

#include <QVector2D>
#include <QtMath>
#include <QDebug>

class Planet
{
public:
    Planet();
    Planet(float x,float y,float m);

    void interact(Planet & other);
    float calculateForce(float distance, float othermass);
    void applyVelocity();


    QVector2D getLocation() const;

    float getMass() const;

    void setMass(float value);

private:
    void applyForceFromOtherPlanet(float force, QVector2D otherLocation);

    float mass;

    QVector2D velocity;
    QVector2D location;
    const float minDistance = 110.f;
    const float constant = 0.05f; //constants should always be declared const
};

#endif // PLANET_H
