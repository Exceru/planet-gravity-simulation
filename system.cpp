#include "system.h"

System::System()
{

}

void System::step()
{
    scene.clear();

    // Every Planet needs to do something for every planet = 2 for loops
    for (size_t i = 0; i < planets.size(); i++) {
        Planet& p = planets[i];

        for (size_t j = i+1; j < planets.size(); j++){
            Planet& p2 = planets[j];
            p.interact(p2);
        }
        p.applyVelocity();
        QVector2D location = p.getLocation();
        double halfMass = static_cast<double>(p.getMass())/2.0;
        scene.addEllipse(static_cast<double>(location.x()),static_cast<double>(location.y()),halfMass,halfMass,QPen(QColor(255,0,0)),QBrush(QColor(0,0,0)));
    }

}

void System::createSystem()
{
    // Clear scene
    scene.clear();
    planets.clear();

    srand(static_cast<unsigned int>(settings.seed));

    qDebug() << settings.amount << "Planets spawned";

    // For amount of planets
    for(size_t i = 1; i <= settings.amount; i++){
        planets.emplace_back(rand() % 600 + (-300),
                rand() % 600 + (-300),
                rand() % (100 - 10 + 1) + 10);
    }
}

SystemSettings *System::getSettings()
{
    return &settings;
}

std::vector<Planet> *System::getPlanets()
{
    return &planets;
}

QGraphicsScene *System::getScene()
{
    return &scene;
}

