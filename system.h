#ifndef SYSTEM_H
#define SYSTEM_H

#include <vector>

#include <QVector2D>
#include <QGraphicsScene>
#include <cstdlib>
#include <QDebug>

#include "planet.h"

struct SystemSettings{
    size_t amount, seed, timescale;
    SystemSettings(): amount(2), seed(0), timescale(1) {}
};

class System :public QObject
{
    Q_OBJECT
public:
    System();

    void step();
    void createSystem();

    SystemSettings* getSettings();
    std::vector<Planet>* getPlanets();
    QGraphicsScene* getScene();


private:
    SystemSettings settings;
    std::vector<Planet> planets;

    QGraphicsScene scene;
};

#endif // SYSTEM_H
